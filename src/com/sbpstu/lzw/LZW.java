package com.sbpstu.lzw;

import com.sbpstu.config.*;
import com.sbpstu.result.Result;

import java.io.*;

public class LZW {
    public static Result run(Config cnf){
        Result result;
        if (cnf.getMode().equals(Grammar.COMPRESS)){
            FileInputStream fileInputStream;
            try {
                fileInputStream = new FileInputStream(cnf.getInputFileName());
            } catch (FileNotFoundException e) {
                return Result.NO_INPUT_FILE;
            }
            FileWriter fileOutWriter;
            try {
                fileOutWriter = new FileWriter(cnf.getOutputFileName());
            } catch (IOException e) {
                return Result.NO_OUTPUT_FILE;
            }
            result = LZWExecutor.encode(fileInputStream, fileOutWriter);
            try {
                fileInputStream.close();
            } catch (IOException e) {
                return Result.ERROR_ClOSE;
            }

            try {
                fileOutWriter.close();
            } catch (IOException e) {
                return Result.ERROR_ClOSE;
            }
            return result;
        }
        else if (cnf.getMode().equals(Grammar.DECOMPRESS)){
            BufferedReader fileInReader;
            try {
                fileInReader = new BufferedReader(new FileReader(cnf.getInputFileName()));
            } catch (FileNotFoundException e) {
                return Result.NO_INPUT_FILE;
            }
            FileWriter fileOutWriter;
            try {
                fileOutWriter = new FileWriter(cnf.getOutputFileName());
            } catch (IOException e) {
                return Result.NO_OUTPUT_FILE;
            }
            result = LZWExecutor.decode(fileInReader, fileOutWriter);

            try {
                fileOutWriter.close();
                fileInReader.close();
            } catch (IOException e) {
                return Result.ERROR_ClOSE;
            }

            return result;
        }
        else {
            return Result.ERROR_INVALID_CONFIG;
        }
    }
}
