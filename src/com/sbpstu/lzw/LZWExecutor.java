package com.sbpstu.lzw;

import com.sbpstu.result.Result;

import java.io.*;
import java.math.BigInteger;
import java.util.HashMap;

/**
 * Class with compression and decompression methods. uses lzw algorithm
 */
class LZWExecutor {
    /**
     * compresses the entered text
     * with the lzw algorithm without a fixed dictionary length and
     * without the maximum word length in the dictionary
     *
     * @param fileInputStream input file
     * @param fileOutWriter output file
     * @return an enumeration that contains the error code and its description or success message
     */
    static Result encode(FileInputStream fileInputStream, FileWriter fileOutWriter){
        //character checksum
        int checksum = 0;

        HashMap<String, Integer> dict = new HashMap<>();
        StringBuilder initDict = new StringBuilder();

        //add all single symbols to dictoniaty
        try {
            for ( int sym = fileInputStream.read(); sym != -1; sym = fileInputStream.read()){
                if (dict.get(Character.toString((char) sym)) == null){
                    dict.put(Character.toString((char) sym), dict.size());
                    initDict.append((char) sym);
                }
                checksum++;
            }
        } catch (IOException e) {
            return Result.ERROR_READ;
        }

        //reset input file
        try {
            fileInputStream.getChannel().position(0);
        } catch (IOException e) {
            return Result.ERROR_READ;
        }

        //write init dictionary and checksum to file
        try {
            fileOutWriter.write(initDict.length()+ "\n" + checksum + "\n" + initDict.toString());
        } catch (IOException e) {
            return Result.ERROR_WRITE;
        }

        int codeBitLen = (int) Math.ceil(Math.log(dict.size()+1) / Math.log(2));

        StringBuilder bitSeqBuiled = new StringBuilder();
        StringBuilder charSeqBuilder = new StringBuilder();
        Result result;
        try {
            for (int nextChar = fileInputStream.read(); nextChar != -1; nextChar = fileInputStream.read()){
                charSeqBuilder.append((char) nextChar);
                Integer code = dict.get(charSeqBuilder.toString());
                if (code == null){
                    bitSeqBuiled.append(
                            getBitStr(dict.get(charSeqBuilder.substring(0, charSeqBuilder.length() - 1)), codeBitLen));
                    //can give a few bytes
                    if (bitSeqBuiled.length() >=8 ){
                        if (!(result = writeBytesToFile(bitSeqBuiled, fileOutWriter)).equals(Result.OK)){
                            return result;
                        }
                    }
                    //add new sequence to dictionary
                    dict.put(charSeqBuilder.toString(), dict.size());
                    codeBitLen = (int) Math.floor(Math.log(dict.size()) / Math.log(2) + 1);

                    charSeqBuilder.setLength(0);
                    charSeqBuilder.append((char) nextChar);
                }
            }
        } catch (IOException e) {
            return Result.ERROR_READ;
        }

        //output rest of line
        bitSeqBuiled.append(getBitStr(dict.get(charSeqBuilder.toString()), codeBitLen));
        bitSeqBuiled.append("0".repeat(Byte.SIZE - (bitSeqBuiled.length() % Byte.SIZE)));
        if (!(result = writeBytesToFile(bitSeqBuiled, fileOutWriter)).equals(Result.OK)){
            return result;
        }

        return Result.OK;
    }

    /**
     * decompresses the entered text
     * with the lzw algorithm without a fixed dictionary length and
     * without the maximum word length in the dictionary
     *
     * @param fileInReader input file
     * @param fileOutWriter output file
     * @return an enumeration that contains the error code and its description or success message
     */
    static Result decode(BufferedReader fileInReader, FileWriter fileOutWriter){
        int dictSize;
        int checkSum;

        try {
            dictSize = Integer.parseInt(fileInReader.readLine());
            checkSum = Integer.parseInt(fileInReader.readLine());
        } catch (IOException e) {
            return Result.ERROR_READ;
        }

        int codeBitLen = (int) Math.ceil(Math.log(dictSize + 1) / Math.log(2));

        HashMap<Integer, String> dict = new HashMap<>();
        //read init dictionary
        for (int i = 0; i < dictSize; i++){
            try {
                dict.put(dict.size(), Character.toString(fileInReader.read()));
            } catch (IOException e) {
                return Result.ERROR_READ;
            }
        }

        //decode the input text
        StringBuilder beginSeq = new StringBuilder();
        StringBuilder bitStrBuilder = new StringBuilder();
        int curSum = 0;
        try {
            for (int symb = fileInReader.read(); symb != -1 && curSum < checkSum; symb = fileInReader.read()) {
                String endSeq;
                bitStrBuilder.append(getBitStr(symb, Byte.SIZE));
                while (bitStrBuilder.length() >= codeBitLen){
                    if (dict.size()-1 < Integer.parseInt(bitStrBuilder.substring(0, codeBitLen),2)){
                        beginSeq.append(beginSeq.charAt(0));
                        dict.put(dict.size(), beginSeq.toString());
                        endSeq = beginSeq.toString();
                    }
                    else {
                        endSeq = dict.get(Integer.parseInt(bitStrBuilder.substring(0, codeBitLen),2));
                        for (int j = 0; j < endSeq.length();j++){
                            beginSeq.append(endSeq.charAt(j));
                            if (!dict.containsValue(beginSeq.toString())){
                                dict.put(dict.size(), beginSeq.toString());
                                beginSeq.setLength(0);
                                beginSeq.append(endSeq.substring(j));
                                break;
                            }
                        }
                    }
                    bitStrBuilder.delete(0, codeBitLen);
                    try {
                        fileOutWriter.write(endSeq);
                    } catch (IOException e) {
                        return Result.ERROR_WRITE;
                    }
                    if ((curSum += endSeq.length()) >= checkSum){
                        break;
                    }
                    codeBitLen = (int) Math.ceil(Math.log(dict.size()+2) / Math.log(2));
                }
            }
        } catch (IOException e) {
            return Result.ERROR_READ;
        }
        return Result.OK;
    }

    /**
     * write bits like char to file
     *
     * @param bitStringBuilder string consisting of 0 and 1
     * @param out output file
     * @return result
     */
    private static Result writeBytesToFile(StringBuilder bitStringBuilder, FileWriter out){
        try {
            while (bitStringBuilder.length() >= Byte.SIZE){
                out.write((char) new BigInteger(bitStringBuilder.substring(0, Byte.SIZE),2).intValue());
                bitStringBuilder.delete(0, Byte.SIZE);
            }
        } catch (IOException e) {
            return Result.ERROR_WRITE;
        }
        return Result.OK;
    }

    /**
     * creates from a number a bit string of a given length
     *
     * @param code code of sequence from dictionary
     * @param finalLen final length of string code
     * @return binary string
     */
    private static String getBitStr(Integer code, int finalLen){
        StringBuilder strCodeBuilder = new StringBuilder("0".repeat(finalLen));
        String binaryCode = Integer.toBinaryString(code);
        strCodeBuilder.replace(finalLen - binaryCode.length(), finalLen, binaryCode);
        return strCodeBuilder.toString();
    }
}
