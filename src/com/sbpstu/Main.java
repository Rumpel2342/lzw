package com.sbpstu;

import com.sbpstu.logger.Logger;
import com.sbpstu.lzw.LZW;
import com.sbpstu.config.Config;
import com.sbpstu.result.Result;
public class Main {

    public static void main(String[] args) {
        Result resultOfTest;
        if (args.length != 1){
            if (!(resultOfTest = Logger.log(Result.ERROR_PARAM.toString())).equals(Result.OK)){
                System.out.println(resultOfTest);
            }
            System.out.println("No input config file name");
            return;
        }
        Config codeConf = new Config();

        resultOfTest = codeConf.setUp(args[0]);
        if (resultOfTest.equals(Result.OK)){
            resultOfTest = LZW.run(codeConf);
        }
        if (!resultOfTest.equals(Result.OK)){
            if (!(resultOfTest = Logger.log(resultOfTest.toString())).equals(Result.OK)){
                System.out.println(resultOfTest);
            }
        }

        /**test encode process
        resultOfTest = codeConf.setUp("encode.conf");
        if (resultOfTest.equals(Result.OK)){
            resultOfTest = LZW.run(codeConf);
        }
        if (!resultOfTest.equals(Result.OK)){
            System.out.println(
                    (resultOfTest = Logger.log(resultOfTest.toString())).equals(Result.OK) ?
                            "" : resultOfTest);
        }
         */
        /**test decode process
        resultOfTest = codeConf.setUp("decode.conf");
        if (resultOfTest.equals(Result.OK)){
            resultOfTest = LZW.run(codeConf);
        }
        if (!resultOfTest.equals(Result.OK)){
            System.out.println(
                    (resultOfTest = Logger.log(resultOfTest.toString())).equals(Result.OK) ?
                            "" : resultOfTest);
        }
         */
    }
}
