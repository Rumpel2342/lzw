package com.sbpstu.config;

public enum Grammar {
    CFG_MODE("MODE"),
    COMPRESS("COMPRESS"),
    DECOMPRESS("DECOMPRESS"),
    CFG_INPUT("INPUT"),
    CFG__OUTPUT("OUTPUT"),
    CFG_EQ("="),
    CFG_SPACE("\\s");


    private String keyWord;

    Grammar(String keyWord){this.keyWord = keyWord;}

    public String toString() {
        return this.keyWord;
    }
}
