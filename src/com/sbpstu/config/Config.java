package com.sbpstu.config;

import com.sbpstu.result.Result;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class Config {
    private static Grammar mode;
    private static String inputFileName;
    private static String outputFileName;

    //static init
    static {
        mode = Grammar.COMPRESS;
    }

    public Grammar getMode() {
        return mode;
    }

    public String getInputFileName() {
        return inputFileName;
    }

    public String getOutputFileName() { return outputFileName; }

    /**
     * read configs from config file
     *
     * @param filename contains configs with a given grammar
     * @return an enumeration that contains the error code and its description or success message
     */
    public Result setUp(String filename) {

        BufferedReader bufferedReader;
        try {
            bufferedReader = new BufferedReader(new FileReader(filename));
        } catch (FileNotFoundException e) {
            return Result.NO_CONFIG;
        }

        try {
            for (String line = bufferedReader.readLine(); line != null; line = bufferedReader.readLine()){
                line = line.replaceAll(Grammar.CFG_SPACE.toString(), "");

                if (!line.contains(Grammar.CFG_EQ.toString())){
                    bufferedReader.close();
                    return Result.MISSING_EQ;
                }

                String[] config = line.split(Grammar.CFG_EQ.toString());

                if (config[0].equals(Grammar.CFG_MODE.toString())){
                    if (config[1].equals(Grammar.COMPRESS.toString())){
                        mode = Grammar.COMPRESS;
                    }
                    else if(config[1].equals(Grammar.DECOMPRESS.toString())){
                        mode = Grammar.DECOMPRESS;
                    }
                    else {
                        bufferedReader.close();
                        return Result.INVALID_MODE;
                    }
                }
                else if (config[0].equals(Grammar.CFG_INPUT.toString())){
                    inputFileName = config[1];
                }
                else if (config[0].equals(Grammar.CFG__OUTPUT.toString())){
                    outputFileName = config[1];
                }
                else {
                    bufferedReader.close();
                    return Result.NO_SUCH_PARAM;
                }
            }
        } catch (IOException e) {
            return Result.ERROR_READ;
        }
        try {
            bufferedReader.close();
        } catch (IOException e) {
            return Result.ERROR_ClOSE;
        }
        return Result.OK;
    }
}
