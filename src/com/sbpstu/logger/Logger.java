package com.sbpstu.logger;

import com.sbpstu.result.Result;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {
    private static String logFileName;
    static {
        logFileName = "error.log";
    }

    public static Result log(String message){
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(logFileName, true));
            bufferedWriter.write(
                    new SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
                            .format(new Date(System.currentTimeMillis())) + "|" + message + "\n");
            bufferedWriter.close();
        } catch (IOException e) {
            return Result.ERROR_LOG;
        }
        return Result.OK;
    }
}
