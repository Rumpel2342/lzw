package com.sbpstu.result;

public enum Result{
    OK("successful run"),
    ERROR_PARAM("Incorrect number of input parameters. " +
            "It Should be 1 parameter contains the name of the config file"),
    NO_CONFIG("no such config file"),
    MISSING_EQ("missing sign \"=\" in config file"),
    INVALID_MODE("no such option, use COMPRESS or DECOMPRESS"),
    NO_SUCH_PARAM("errors in the config file, invalid config field"),
    NO_INPUT_FILE("error in open input file"),
    NO_OUTPUT_FILE("error in open input file"),
    ERROR_READ("error while reading file"),
    ERROR_WRITE("error while writing to the file"),
    ERROR_ClOSE("error while closing file or file stream"),
    ERROR_LOG("can not open log file"),
    ERROR_INVALID_CONFIG("error in config");

    private String message;

    Result(String message) {
        this.message = message;
    }

    public String toString() {
        return this.message;
    }
}
